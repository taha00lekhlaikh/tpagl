package ma.ac.emi.agl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpAglApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpAglApplication.class, args);
	}

}
